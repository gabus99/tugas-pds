<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Name : ";
echo $sheep->get_name()."<br>"; 
echo "Legs : ";
echo $sheep->get_legs()."<br>"; 
echo "Cold Blooded : ";
echo $sheep->get_cold_blooded()."<br>";
echo "<br>";

$kodok = new frog("buduk");
echo "Name : ". $kodok->get_name(). "<br>";
echo "Legs : ". $kodok->get_legs(). "<br>";
echo "Cold Blooded : ". $kodok->get_cold_blooded(). "<br>";
echo "Jump : ". $kodok-> jump(). "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->get_name(). "<br>";
echo "Legs : ". $sungokong->get_legs(). "<br>";
echo "Cold Blooded : ". $sungokong->get_cold_blooded(). "<br>";
echo "Yell : ". $sungokong-> yell(). "<br>";
?>